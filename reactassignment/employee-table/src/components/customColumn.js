import { Component } from "react";
import { Column } from 'primereact/column';

class customColumn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            field: String,
            header: String
        };
    }
    render() {
        return (
            <Column {...this.state.props} />
        )
    }
}
export default customColumn;