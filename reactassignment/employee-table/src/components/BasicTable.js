import React, { Component, Fragment } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash, faPen, faVenus, faMars } from '@fortawesome/free-solid-svg-icons';
import { ViewEmployee } from './ViewEmployee';
import { EditEmployee } from './EditEmployee';
import { DeleteEmployee } from './DeleteEmployee';
import Datatable from './Datatable';
import Data from './emp_details.json';
import 'primereact/resources/themes/saga-blue/theme.css';
import 'primeicons/primeicons.css';
import 'bootstrap/dist/css/bootstrap.min.css';
export class BasicTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewModalShow: false,
            editModalShow: false,
            deleteModalShow: false,
            empname: '',
            empid: '',
            tableValue: '',
            rowdata: ''
        };
        this.handleinputvalue = this.handleInputValue.bind(this);
    }
    handleInputValue(val) {
        let empname = this.state.empname;
        empname.push(val);
        this.setState({ empname: empname });
    }
    componentDidMount() {
        this.refreshList();
    }
    refreshList() {
        localStorage.setItem('data', JSON.stringify(Data));
    }
    render() {
        var tableValue = JSON.parse(localStorage.getItem('data'));
        console.log("local storage is: ", tableValue);

        const nameBodyTemplate = (rowData) => {
            var emp_name = rowData['Last Name'] + " " + rowData['First Name']
            return <a href="#"
                onClick={() => this.setState({
                    viewModalShow: true, empid: rowData['Emp ID'], empname: emp_name, emp_gender: rowData.Gender,
                    emp_email: rowData['E Mail'], emp_phone: rowData['Phone No.'], emp_doj: rowData['Date of Joining'], emp_salary: rowData.Salary
                })}>
                {emp_name}</a >
        }
        const emailBodyTemplate = (rowData) => {
            return <a href={"mailto:" + rowData['E Mail']}>{rowData['E Mail']}</a>
        }
        const phoneBodyTemplate = (rowData) => {
            return (<b>{rowData["Phone No."]}</b>);
        }
        const dojBodyTemplate = (rowData) => {
            var fromdate = new Date(rowData["Date of Joining"]);
            var dd = fromdate.getDate();
            var mm = fromdate.getMonth() + 1; //January is 0
            var yyyy = fromdate.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            var fromdate1 = dd + '-' + mm + '-' + yyyy;
            return fromdate1;
        }
        const genderBodyTemplate = (rowData) => {
            if (rowData.Gender === 'F') {
                var gender = <FontAwesomeIcon icon={faVenus} />
            } else {
                gender = <FontAwesomeIcon icon={faMars} />
            }
            return gender;
        }
        const hikeBodyTemplate = (rowData) => {
            return (Math.round(rowData.Salary - ((rowData.Salary * 100) / (100 + +rowData['Last % Hike'].replace("%", "")))))
        }
        const actionBodyTemplate = (rowData) => {
            return (
                <React.Fragment>
                    <FontAwesomeIcon icon={faPen} style={{ color: "#f0ad4e" }} className="faPen"
                        onClick={() => this.setState({ editModalShow: true, empid: rowData['Emp ID'], empname: rowData.emp_name, emp_email: rowData['E Mail'] })} />
                    <FontAwesomeIcon className="ml-2" icon={faTrash} style={{ color: "#d9534f" }} className="faTrash"
                        onClick={() => this.setState({ deleteModalShow: true, empid: rowData['Emp ID'] })} />
                </React.Fragment>
            );
        }
        const { empid, empname, emp_email, emp_doj, emp_gender, emp_salary, emp_phone } = this.state;
        let viewModalClose = () => this.setState({ viewModalShow: false });
        let editModalClose = () => this.setState({ editModalShow: false });
        let deleteModalClose = () => this.setState({ deleteModalShow: false });
        return (
            <Fragment>
                <div>
                    <Datatable tableValue={tableValue} >
                        <customColumn field="Emp ID" header="Employee ID" />
                        <customColumn header="Employee Name" body={nameBodyTemplate} />
                        <customColumn field="Date of Joining" header="Date of Joining" body={dojBodyTemplate} />
                        <customColumn field="E Mail" header="E Mail" body={emailBodyTemplate} />
                        <customColumn field="Gender" header="Gender" body={genderBodyTemplate} />
                        <customColumn header="Phone No." body={phoneBodyTemplate} />
                        <customColumn field="Last % Hike" header="Last Hike Amount" body={hikeBodyTemplate} />
                        <customColumn field="Salary" header="Salary" sortable />
                        <customColumn header="Action" body={actionBodyTemplate} />
                    </Datatable>
                </div>
                <ViewEmployee show={this.state.viewModalShow} onHide={viewModalClose} empid={empid} empname={empname} emp_gender={emp_gender} emp_email={emp_email} emp_phone={emp_phone} emp_doj={emp_doj} emp_salary={emp_salary} />
                <EditEmployee show={this.state.editModalShow} onHide={editModalClose} empid={empid} empname={empname} emp_email={emp_email} />
                <DeleteEmployee show={this.state.deleteModalShow} onHide={deleteModalClose} empid={empid} />
            </Fragment >
        )
    }
}

export default BasicTable
