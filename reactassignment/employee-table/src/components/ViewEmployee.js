import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export class ViewEmployee extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Modal {...this.props}
                    aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Employee Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="emp_id">Employee ID: <span><b> {this.props.empid}</b></span></div>
                            <div className="name">Name: <span><b> {this.props.empname}</b></span></div>
                            <div className="gender">Gender: <span><b> {this.props.emp_gender}</b></span></div>
                            <div className="email">Email: <span><b> {this.props.emp_email}</b></span></div>
                            <div className="phone">Phone Number: <span><b> {this.props.emp_phone}</b></span></div>
                            <div className="date">Date of Joining: <span><b> {this.props.emp_doj}</b></span></div>
                            <div className="salary">Salary: <span><b> {this.props.emp_salary}</b></span></div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.props.onHide}>Close</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default ViewEmployee;
