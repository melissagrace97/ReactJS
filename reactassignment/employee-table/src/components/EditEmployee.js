import React, { Component } from 'react'
import { Modal, Button, Row, Col, Form } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export class EditEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = { empname: '', empid: '', tableValue: '' };
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(event) {
        let empname = this.state.empname;
        this.setState({ empname: event.target.value });
        console.log("updated name is: ", empname)
    };
    render() {
        const { empname } = this.props;
        return (
            <div className="container">
                <Modal {...this.props} aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Edit Employee Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <Row>
                                <Col>
                                    <Form onSubmit={this.handleFormSubmit}>
                                        <Form.Group controlId="Emp ID">
                                            <Form.Label>Employee ID</Form.Label>
                                            <Form.Control
                                                type="text"
                                                name="EmployeeID"
                                                disabled
                                                defaultValue={this.props.empid}
                                                placeholder="Employee ID"
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="emp_name">
                                            <Form.Label>Employee Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                name="empname"
                                                placeholder="Employee Name"
                                                value={this.state.empname}
                                                onChange={this.handleChange}
                                                defaultValue={this.props.empname}
                                            />
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={this.props.onHide}>Close</Button>
                        <Button variant="success" type="submit" onClick={this.props.onHide} >Save</Button>
                    </Modal.Footer>
                </Modal>
            </div >
        )
    }
}

export default EditEmployee;
