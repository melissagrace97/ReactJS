import React, { Component } from 'react';
import BasicTable from './components/BasicTable';
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-dark bg-primary fixed-top"><span className="navbar-brand"><b>Employee Management</b></span></nav>
        <div className="pt-5 mt-5 px-5">
          <BasicTable />
        </div>
      </div>
    )
  }
}

export default App;